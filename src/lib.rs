use wasm_bindgen::prelude::wasm_bindgen;

#[wasm_bindgen]
pub fn start(root_element: web_sys::Element) {
  let mut count = 0;
  let app = smithy::smd!(
    <div on_click={|_| count = count + 1}>
      I have been clicked {count} times.
    </div>
  );

  smithy::mount(Box::new(app), root_element);
}